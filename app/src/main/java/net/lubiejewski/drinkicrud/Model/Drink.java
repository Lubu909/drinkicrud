package net.lubiejewski.drinkicrud.Model;

/**
 * Created by Lubu909 on 2016-12-16.
 */

public class Drink {
    int id;
    String nazwa;
    String przepis;

    public Drink() {
    }

    public Drink(String nazwa, String przepis) {
        this.nazwa = nazwa;
        this.przepis = przepis;
    }

    public Drink(int id, String nazwa, String przepis) {
        this.id = id;
        this.nazwa = nazwa;
        this.przepis = przepis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getPrzepis() {
        return przepis;
    }

    public void setPrzepis(String przepis) {
        this.przepis = przepis;
    }
}
