package net.lubiejewski.drinkicrud;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.lubiejewski.drinkicrud.Model.Drink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import static net.lubiejewski.drinkicrud.R.id.fab;

public class DrinkLista extends AppCompatActivity {

    ArrayList<Drink> drinkArray;
    private String tag = MainActivity.class.getSimpleName();

    private ProgressDialog dialog;
    private ListView list;

    private String APIurl = "https://lubiejewski.net/drinkiAPI/drink";
    private String authToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_lista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ZMIENIC POZNIEJ
        authToken = "Bearer " + "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzLCJleHAiOjE0ODI0Mjc2NDJ9.AJlvKWjKtW2W7LIutdv4C3om1FmSSCG90l9caM0G8qA";
        //---------------

        new getList().execute();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class getList extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(DrinkLista.this);
            dialog.setMessage("Pobieranie danych");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            URL url = null;
            InputStream is = null;
            JSONObject json;
            try {
                url = new URL(APIurl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Authorization",authToken);
                conn.connect();
                int responseCode = conn.getResponseCode();
                Log.i("Connection", "HTTP Response code : " + responseCode);
                if(responseCode == 200){
                    //InputStream in = new BufferedInputStream(conn.getInputStream());
                    //json = new JSONObject(getResponseText(in));

                    JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));

                    /*
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                                                TUTAJ SYPIE
                     */

                            //json.getJSONArray("drink");
                    for(int i=0; i<array.length();i++){
                        JSONObject obiekt = array.getJSONObject(i);
                        Drink drink = new Drink();
                        drink.setId(obiekt.getInt("id_drink"));
                        drink.setNazwa(obiekt.getString("nazwa"));
                        drinkArray.add(drink);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            dialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    /*
                    ListAdapter adapter = new SimpleAdapter(
                            DrinkLista.this, drinkArray,
                            R.layout.item_drink, new String[] { "id_drink", "nazwa"},
                            new int[] { R.id.id_drinka, R.id.nazwa_drinka });
                    */
                    // updating listview
                    list = (ListView) findViewById(R.id.lista_drink);
                    //list.setAdapter(adapter);
                }
            });
        }

        private String getResponseText(InputStream inStream) {
            // very nice trick from
            // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
            return new Scanner(inStream).useDelimiter("\\A").next();
        }
    }

}
